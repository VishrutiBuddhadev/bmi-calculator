let ans=document.getElementById('ans');
let addbutton=document.getElementById('addbtn');
addbutton.addEventListener('click',()=>{
    let weight=document.getElementById('weight').value;
    let height=document.getElementById('height').value;
    // let ans=document.getElementById('ans');
    if (isNaN(weight) || isNaN(height) || weight<0 || height<0 || weight==""||height=="") {
        alert("Please enter valid values for weight and height.");
    }
    else{
        let bmi=weight/(height*height);
        let category;
        if(bmi<18.5)
        {
            category="Underweight.Consume more calories and protein.";
        }
        else if(bmi<24.9)
        {
            category="You are in a healthy weight range. Maintain a balanced diet.";
        }
        else if(bmi<29.9)
        {
            category="You are in a healthy weight range. Maintain a balanced diet.";
        }
        else{
            category="Obese.Consult a dietitian for a personalized plan";
        }
        ans.innerHTML='Your BMI is : '+bmi.toFixed(2)+". You Are "+category;
        // alert('Your BMI is '+bmi.toFixed(2)+". "+category);
    
    }

}) 


